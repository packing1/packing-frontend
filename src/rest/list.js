import axios from 'axios';

const getAllList = async () => {
  const response = await axios.get('/api/list');
  return response;
};

const getAllStage = async () => {
  const response = await axios.get('/api/stage');
  return response;
};

const getList = async (id) => {
  const response = await axios.get(`/api/list/${id}`);
  return response;
};

const addTodo = async (name, listId, stageId) => {
  const data = {
    name,
    listId,
    stageId,
  };
  const response = await axios.post('/api/todo', data);
  return response;
};

const deleteTodo = async (id) => {
  const response = await axios.delete(`/api/todo/${id}`);
  return response;
};

const updateTodo = async (id, data) => {
  const response = await axios.patch(`/api/todo/${id}`, data);
  return response;
};

export { getAllList, getList, addTodo, deleteTodo, updateTodo, getAllStage };
