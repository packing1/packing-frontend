import { Route, BrowserRouter, Routes } from 'react-router-dom';
import Lists from './components/lists/Lists';
import List from './components/lists/List';

const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Lists />} />
        </Routes>
        <Routes>
          <Route path="/:id" element={<List />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
