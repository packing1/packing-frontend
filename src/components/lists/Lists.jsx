import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import AddTodo from '../stage/AddTodo';
import { getAllList, getAllStage } from '../../rest/list';

const Lists = () => {
  const [lists, setLists] = useState([]);
  const [stages, setStages] = useState([]);
  const [open, setOpen] = useState(false);
  const [newList, setNewList] = useState('');
  const [newStageIds, setStageIds] = useState([]);

  const fetchData = async () => {
    const response = await getAllList();
    setLists(response.data.data);
  };

  const fetchStageData = async () => {
    const response = await getAllStage();
    setStages(response.data.data);
  };

  useEffect(() => {
    fetchData();
    fetchStageData();
  }, []);

  const handleOpenDialog = () => {
    setOpen(true);
  };

  const handleCloseDialog = () => {
    setOpen(false);
  };

  const handleTextChange = (event) => {
    setNewList(event.target.value);
  };

  const handleNewStagesChange = (event) => {
    setStageIds((prevState) => {
      if (prevState.includes(event.target.value)) {
        console.log('kivesz');
        console.log(event.target.value);
        prevState.filter((id) => id === event.target.value);
      } else {
        prevState.push(event.target.value);
      }
      console.log(prevState);
      return prevState;
    });
  };

  return (
    <div className="lists-wrapper">
      <img src="/backpack.png" alt="backpack" />
      <div className="add-list">
        <AddTodo openDialog={handleOpenDialog} />
      </div>
      <div className="lists">
        {lists.map((list) => (
          <Accordion key={list._id} className="lists-item">
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
              className="lists-item-header"
            >
              <Link to={`/${list._id}`}>{list.name}</Link>
            </AccordionSummary>
            <AccordionDetails>
              {stages.length > 0 &&
                list.stages.map((id) => (
                  <div className="stage-name">
                    {stages.find((stage) => stage._id === id).name}
                  </div>
                ))}
            </AccordionDetails>
          </Accordion>
        ))}
      </div>
      <Dialog
        className="add-list-dialog"
        onClose={handleCloseDialog}
        open={open}
      >
        <DialogTitle>Lista hozzáadása</DialogTitle>
        <DialogContent>
          <TextField
            id="new-list"
            variant="standard"
            onChange={handleTextChange}
            value={newList}
            placeholder="Név"
          />
          <div className="stages">
            <FormGroup onChange={handleNewStagesChange}>
              {stages.length > 0 &&
                stages.map((stage) => (
                  <FormControlLabel
                    control={<Checkbox value={stage._id} />}
                    label={stage.name}
                  />
                ))}
            </FormGroup>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog}>Mégse</Button>
          <Button onClick={() => {}}>Hozzáad</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default Lists;
