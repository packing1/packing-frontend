import { useEffect, useState } from 'react';
import _ from 'lodash';
import { useParams } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import { DragDropContext } from 'react-beautiful-dnd';
import { getList, addTodo, deleteTodo, updateTodo } from '../../rest/list';
import useMediaQuery from '@mui/material/useMediaQuery';

import Stage from '../stage/Stage';
import MobileStage from '../stage/MobileStage';

const Lists = () => {
  const [listData, setData] = useState(null);
  const [moving, setMoving] = useState(false);
  const isDesktop = useMediaQuery('(min-width:680px)');
  const { id } = useParams();

  const fetchData = async () => {
    const response = await getList(id);
    setData(response.data.data);
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line
  }, []);

  const handleAddTodo = async (name, stageId) => {
    const newTodo = await addTodo(name, id, stageId);

    const { stageName } = Object.values(listData.todos).find(
      (stage) => stage.stageId === stageId
    );

    // REFACTOR
    setData((prevData) => ({
      ...prevData,
      todos: {
        ...prevData.todos,
        [stageName]: {
          ...prevData.todos[stageName],
          todos: [newTodo.data.data, ...prevData.todos[stageName].todos],
        },
      },
    }));
  };

  const handleDeleteTodo = async (id, stageId) => {
    await deleteTodo(id);

    const { stageName } = Object.values(listData.todos).find(
      (stage) => stage.stageId === stageId
    );

    // REFACTOR
    setData((prevData) => ({
      ...prevData,
      todos: {
        ...prevData.todos,
        [stageName]: {
          ...prevData.todos[stageName],
          todos: prevData.todos[stageName].todos.filter(
            (todo) => todo._id !== id
          ),
        },
      },
    }));
  };

  const handleDragStart = () => {
    setMoving(true);
  };

  const handleDragEnd = async (data) => {
    const sourceStage = Object.values(listData.todos).find(
      (stage) => stage.stageId === data.source.droppableId
    ).stageName;

    const destinationStage =
      data.destination &&
      Object.values(listData.todos).find(
        (stage) => stage.stageId === data.destination.droppableId
      ).stageName;

    if (destinationStage && destinationStage !== sourceStage) {
      const movingTodo = listData.todos[sourceStage].todos.find(
        (todo) => todo._id === data.draggableId
      );

      setData((prevData) => ({
        ...prevData,
        todos: {
          ...prevData.todos,
          [sourceStage]: {
            ...prevData.todos[sourceStage],
            todos: prevData.todos[sourceStage].todos.filter(
              (todo) => todo._id !== data.draggableId
            ),
          },
          [destinationStage]: {
            ...prevData.todos[destinationStage],
            todos: [movingTodo, ...prevData.todos[destinationStage].todos],
          },
        },
      }));
      const newData = { stageId: data.destination.droppableId };
      await updateTodo(data.draggableId, newData);
    }
    setMoving(false);
  };

  const handleUpdateTodoStatus = async (id, status) => {
    const data = { status };
    const updatedTodo = await updateTodo(id, data);
    const stageName = Object.values(listData.todos).find(
      (stage) => stage.stageId === updatedTodo.data.data.todo.stageId
    ).stageName;

    const newData = _.cloneDeep(listData);

    const todoIndex = listData.todos[stageName].todos.findIndex(
      (todo) => todo._id === updatedTodo.data.data.todo._id
    );

    newData.todos[stageName].todos[todoIndex] = updatedTodo.data.data.todo;

    setData(newData);
  };

  return (
    <DragDropContext onDragEnd={handleDragEnd} onDragStart={handleDragStart}>
      <div className="list-wrapper">
        {!!listData && (
          <div className="content">
            <Typography variant="h2" component="h2">
              {listData.list.name}
            </Typography>
            <Divider />
            <div className={isDesktop ? 'stages' : 'mobile-stage'}>
              {Object.keys(listData.todos)
                .sort()
                .map((stageName, index) => {
                  return isDesktop ? (
                    <Stage
                      key={index}
                      stage={listData.todos[stageName]}
                      onAddNewTodo={handleAddTodo}
                      onDeleteTodo={handleDeleteTodo}
                      isMovingTodo={moving}
                      updateTodoStatus={handleUpdateTodoStatus}
                    />
                  ) : (
                    <MobileStage
                      key={index}
                      stage={listData.todos[stageName]}
                      onAddNewTodo={handleAddTodo}
                      onDeleteTodo={handleDeleteTodo}
                      updateTodoStatus={handleUpdateTodoStatus}
                    />
                  );
                })}
            </div>
          </div>
        )}
      </div>
    </DragDropContext>
  );
};

export default Lists;
