import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';

const AddTodo = (props) => {
  return (
    <div className="add-todo">
      <Paper elevation={3}>
        <IconButton aria-label="add" color="primary" onClick={props.openDialog}>
          <AddCircleOutlineIcon />
        </IconButton>
      </Paper>
    </div>
  );
};

export default AddTodo;
