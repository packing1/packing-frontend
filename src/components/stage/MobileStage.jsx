import { useState } from 'react';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import MobileTodo from './MobileTodo';
import AddTodo from './AddTodo';

const Stage = (props) => {
  const [open, setOpen] = useState(false);
  const [newTodo, setNewTodo] = useState('');

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleTextChange = (event) => {
    setNewTodo(event.target.value);
  };

  const handleAddNewTodo = () => {
    props.onAddNewTodo(newTodo, props.stage.stageId);
    setOpen(false);
    setNewTodo('');
  };

  return (
    <div className="mobile-stage-wrapper">
      <Typography variant="h4" component="h4">
        {props.stage.stageName}
      </Typography>
      <Divider />
      <AddTodo openDialog={handleClickOpen} />

      {props.stage.todos.map((todo, index) => (
        <MobileTodo
          key={index}
          todo={todo}
          onDeleteTodo={props.onDeleteTodo}
          index={index}
          updateTodoStatus={props.updateTodoStatus}
        />
      ))}

      <Dialog className="add-dialog" onClose={handleClose} open={open}>
        <DialogTitle>Elem hozzáadása</DialogTitle>
        <DialogContent>
          <TextField
            id="new-todo"
            variant="standard"
            onChange={handleTextChange}
            value={newTodo}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Mégse</Button>
          <Button onClick={handleAddNewTodo}>Hozzáad</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default Stage;
