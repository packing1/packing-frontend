import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import HelpIcon from '@mui/icons-material/Help';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const Todo = (props) => {
  const getIcon = (status) => {
    if (status === 'maybe') return <HelpIcon />;
    if (status === 'shopping') return <ShoppingCartIcon />;
    if (status === 'ready') return <CheckCircleIcon />;
  };

  return (
    <Accordion className="mobile-todo">
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <div className="accordion-header">
          <div>
            <Typography variant="h5" component="h5">
              {props.todo.name}
            </Typography>
          </div>
          <div>{getIcon(props.todo.status)}</div>
        </div>
      </AccordionSummary>
      <AccordionDetails>
        <Typography className="status-wrapper" variant="body2" component="div">
          <IconButton
            aria-label="maybe"
            color={props.todo.status === 'maybe' ? 'primary' : undefined}
            onClick={() => props.updateTodoStatus(props.todo._id, 'maybe')}
          >
            <HelpIcon />
          </IconButton>
          <IconButton
            aria-label="shopping"
            color={props.todo.status === 'shopping' ? 'primary' : undefined}
            onClick={() => props.updateTodoStatus(props.todo._id, 'shopping')}
          >
            <ShoppingCartIcon />
          </IconButton>
          <IconButton
            aria-label="ready"
            color={props.todo.status === 'ready' ? 'primary' : undefined}
            onClick={() => props.updateTodoStatus(props.todo._id, 'ready')}
          >
            <CheckCircleIcon />
          </IconButton>
        </Typography>

        <Button
          size="small"
          onClick={() => props.onDeleteTodo(props.todo._id, props.todo.stageId)}
          variant="danger"
        >
          Törlés
        </Button>
      </AccordionDetails>
    </Accordion>
  );
};

export default Todo;
