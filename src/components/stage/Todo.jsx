import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import HelpIcon from '@mui/icons-material/Help';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Draggable } from 'react-beautiful-dnd';

const Todo = (props) => {
  return (
    <Draggable draggableId={props.todo._id} index={props.index}>
      {(provided) => (
        <div
          className="todo"
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
        >
          <Card>
            <CardContent>
              <Typography variant="h5" component="h5">
                {props.todo.name}
              </Typography>
              <Typography
                className="status-wrapper"
                variant="body2"
                component="div"
              >
                <IconButton
                  aria-label="maybe"
                  color={props.todo.status === 'maybe' ? 'primary' : undefined}
                  onClick={() =>
                    props.updateTodoStatus(props.todo._id, 'maybe')
                  }
                >
                  <HelpIcon />
                </IconButton>
                <IconButton
                  aria-label="shopping"
                  color={
                    props.todo.status === 'shopping' ? 'primary' : undefined
                  }
                  onClick={() =>
                    props.updateTodoStatus(props.todo._id, 'shopping')
                  }
                >
                  <ShoppingCartIcon />
                </IconButton>
                <IconButton
                  aria-label="ready"
                  color={props.todo.status === 'ready' ? 'primary' : undefined}
                  onClick={() =>
                    props.updateTodoStatus(props.todo._id, 'ready')
                  }
                >
                  <CheckCircleIcon />
                </IconButton>
              </Typography>
            </CardContent>
            <CardActions>
              <Button
                size="small"
                onClick={() =>
                  props.onDeleteTodo(props.todo._id, props.todo.stageId)
                }
                variant="danger"
              >
                Törlés
              </Button>
            </CardActions>
          </Card>
        </div>
      )}
    </Draggable>
  );
};

export default Todo;
